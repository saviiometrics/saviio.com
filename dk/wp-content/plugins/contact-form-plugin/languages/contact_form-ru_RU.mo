��    :      �  O   �      �  $   �          0     C     Y  +   e     �     �     �     �  	   �     �     �               /     5  �   9  D   �  o      q   p  
   �     �               &     @     I     N     T     c  3   �  ,   �  	   �     �     �     	     	     ,	  <   C	  ?   �	     �	     �	  *   �	     �	     
     
     $
  /   +
     [
     c
     �
     �
  -   �
     �
  +   �
       �  1  T   "  +   w  /   �  N   �     "  M   A  "   �     �  2   �          %  ;   9  B   u     �     �     �     �  �   �  �   �  �   W  �        
  %   $  )   J     t  @   �     �     �     �     �  4   	  l   >  �   �     ;  C   S     �  0   �  $   �  )   �  �   $  l   �     #     6  K   ?     �  6   �  	   �     �  c   �     L  /   _  *   �  (   �  Y   �     =  Y   ]  ;   �                        9   +      5      )      "       -   7   $   4   2   (      :             !   .          6       #   ,      3                              1       0   	           %             '      
                    *      /              &                                 8    A proper e-mail address is required. Activated plugins Additional options Attachment is broken. Attachment: Change label for fields of the contact form Coming from (referer) Contact Form Contact Form Options Contact from Date/Time Display Attachment block Display Send me a copy block Download E-Mail Address: Email FAQ If information in the below fields are empty then the message will be send to an address which was specified during registration. If you can see this MIME than your client doesn't accept MIME types! If you have any questions, please contact us via plugin@bestwebsoft.com or fill in our contact form on our site If you would like to add a Contact Form to your website, just copy and put this shortcode onto your post or page: Install %s Install now from wordpress.org Installed plugins Message Message text is required. Message: Name Name: Options saved. Please complete the CAPTCHA. Please input correct email. Settings are not saved. Please make corrections below and try again. Read more Recommended plugins Save Changes Select user name Send me a copy Sent from (ip address) Set a name of user wo will get messages from a contact form. Set an email address which will be used for messages receiving. Settings Site Sorry, your e-mail could not be delivered. Subject Subject text is required. Subject: Submit Such user is not exist. Settings are not saved. Support Thank you for contacting us. Use email of wordpress user: Use this email: Users can attach files of the following types Using (user agent) You can attach files of the following types Your name is required. Project-Id-Version: contact_form
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2012-01-04 12:10+0200
PO-Revision-Date: 2012-01-04 12:10+0200
Last-Translator: BWS <zos@bestwebsoft.com>
Language-Team: bestwebsoft.com <plugin@bestwebsoft.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: __;_e
X-Poedit-Basepath: ..
X-Poedit-Language: Russian
X-Poedit-Country: RUSSIAN FEDERATION
X-Poedit-SourceCharset: utf-8
X-Poedit-SearchPath-0: .
 Поле e-mail адреса - обязательное для заполнения. Активированные плагины Дополнительные настройки Прикрепленный тип файла не поддерживается Прикрепить файл: Изменить названия полей  контактной формы Пришло из (реферер) Контактная Форма Настройки Контактной Формы Контактная Форма Дата/Время Отобразить блок Прикрепить файл Отобразить блок Отправить мне копию Скачать E-mail адрес: Email FAQ Если информация в полях ниже отсутствует, соощения будут оправлены на email адрес, который был указан при регистрации сайта. Если вы можете видеть этот MIME значит ваш почтовый клиент не поддерживает MIME тип! Если у вас есть какие-то впросы, обращайтесь на plugin@bestwebsoft.com или заполните контактную форму на нашем сайте Если вы хотите добавить Контактную Форму на свой сайт, вам надо только скопировать и вставить шорткод в контент страницы или поста: Установлено %s Установить с wordpress.org Установленные плагины Сообщение Поле Сообщение - обязательное поле. Сообщение: Имя Имя: Опции сохранены Пожалуйста, заполните КАПЧУ. Пожалуйста, введите корректный email. Настройки не сохранены. Пожалуйста, сделайте исправления в отмеченных полях ниже и повторите попытку. Читать далее Рекомендованные к установке плагины Save Changes Выберите имя пользователя Отправить мне копию Отправлено от (ip адрес) Укажите логин пользователя, который будет получать сообщения контактной формы Укажите email адрес, на который будут отправляться сообщения. Настройки Сайт Извините, ваш email не может быть отправлен. Тема Поле Тема - обязательное поле. Тема: Отправить Данный пользователь не найден. Настройки не сохранены Поддержка Спасибо за контакт с нами. Email пользователя сайта: Использовать этот email: Пользователи могут прикрепить файлы таких типов Используя (user agent) Пользователи могут прикрепить файлы таких типов Ваше имя - это обязательное поле. 