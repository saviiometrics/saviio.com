��    :      �  O   �      �  $   �          0     C     Y  +   e     �     �     �     �  	   �     �     �               /     5  �   9  D   �  o      q   p  
   �     �               &     @     I     N     T     c  3   �  ,   �  	   �     �     �     	     	     ,	  <   C	  ?   �	     �	     �	  *   �	     �	     
     
     $
  /   +
     [
     c
     �
     �
  -   �
     �
  +   �
       �  1  *        C     V     j     �  ;   �     �     �     �               (     :     Q     Z     j     q  x   �  E   	  q   O  �   �     I  #   X     |  	   �  #   �  
   �     �     �     �      �  R     J   d     �     �     �     �     �       N   &  C   u     �     �  B   �       "        :     C  A   J     �     �  2   �     �  .        7  )   I     s                        9   +      5      )      "       -   7   $   4   2   (      :             !   .          6       #   ,      3                              1       0   	           %             '      
                    *      /              &                                 8    A proper e-mail address is required. Activated plugins Additional options Attachment is broken. Attachment: Change label for fields of the contact form Coming from (referer) Contact Form Contact Form Options Contact from Date/Time Display Attachment block Display Send me a copy block Download E-Mail Address: Email FAQ If information in the below fields are empty then the message will be send to an address which was specified during registration. If you can see this MIME than your client doesn't accept MIME types! If you have any questions, please contact us via plugin@bestwebsoft.com or fill in our contact form on our site If you would like to add a Contact Form to your website, just copy and put this shortcode onto your post or page: Install %s Install now from wordpress.org Installed plugins Message Message text is required. Message: Name Name: Options saved. Please complete the CAPTCHA. Please input correct email. Settings are not saved. Please make corrections below and try again. Read more Recommended plugins Save Changes Select user name Send me a copy Sent from (ip address) Set a name of user wo will get messages from a contact form. Set an email address which will be used for messages receiving. Settings Site Sorry, your e-mail could not be delivered. Subject Subject text is required. Subject: Submit Such user is not exist. Settings are not saved. Support Thank you for contacting us. Use email of wordpress user: Use this email: Users can attach files of the following types Using (user agent) You can attach files of the following types Your name is required. Project-Id-Version: contact_form
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2012-01-04 12:41+0200
PO-Revision-Date: 2012-01-04 12:41+0200
Last-Translator: BWS <zos@bestwebsoft.com>
Language-Team: Thomas Hartung <thartung@adipositas-mm.de>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: __;_e
X-Poedit-Basepath: ..
X-Poedit-Language: German
X-Poedit-Country: GERMANY
X-Poedit-SourceCharset: utf-8
X-Poedit-SearchPath-0: .
 Eine korrekte E-Mail-Adresse ist zwingend. Aktivierte PlugIns Erweiterte Optionen Anhang scheint defekt zu sein. Anhang: Ändern Sie die Bezeichnung der Felder des Kontaktformulars gesendet von (referer) Contact Form Contact Form Einstellungen Kontakt von Datum / Uhrzeit Anhänge erlauben Kopie-an-mich anzeigen Download E-Mail-Adresse: E-Mail Häufig gestellte Fragen (FAQ) Wenn das folgende Feld leer bleibt wird die Nachricht an die Adresse gesandt, die bei der Registrierung angegeben wurde. Wenn Sie dieses MIME sehen, unterstützt Ihr Client keine MIME-Typen! Bei Fragen wenden Sie sich an plugin@bestwebsoft.com oder füllen Sie das Kontakformular auf unserer Webseite aus Wenn Sie Contact Form in Ihrer Webpräsenz verwenden wollen, kopieren Sie einfach folgenden shortcode auf Ihre Seite oder Ihren Artikel Installiere %s installiere jetzt von wordpress.org Installierte PlugIns Nachricht Bitte geben Sie eine Nachricht ein. Nachricht: Nome Name: Einstellungen gespeichert. Bitte ergänzen Sie das CAPTCHA. Bitte geben Sie eine korrekte E-Mail-Adresse ein. Einstellungen nicht gespeichert. Bitte führen Sie die Korrekturen unten durch und versuchen Sie es erneut. Lesen Sie mehr Empfohlene PlugIns Änderungen speichern Wähle Benutzer Kopie an mich senden Gesendet von (IP-Adresse) Definieren Sie die Benutzer, die Nachrichten von Contact Form bekommen sollen. Definieren Sie die E-Mail-Adresse, die Contact Form verwenden soll. Einstellungen Site Entschuldigung, Ihre E-Mail konnte leider nicht zugestellt werden. Betreff Bitte geben Sie einen Betreff ein. Betreff: senden Dieser Benutzer existiert nicht. Einstellungen nicht gespeichert. Unterstützung Danke für Ihre Nachricht. Benutze die E-Mail-Adresse von WordPress-Benutzer: Verwende diese E-Mail-Adresse: Benutzer können folgende Dateitypen anhängen mit  (user agent) Sie können folgende Dateitypen anhängen Ihr Name wird benötigt 