��    :      �  O   �      �  $   �          0     C     Y  +   e     �     �     �     �  	   �     �     �               /     5  �   9  D   �  o      q   p  
   �     �               &     @     I     N     T     c  3   �  ,   �  	   �     �     �     	     	     ,	  <   C	  ?   �	     �	     �	  *   �	     �	     
     
     $
  /   +
     [
     c
     �
     �
  -   �
     �
  +   �
       �  1  )        ?     O     g     �  0   �     �     �     �     �     �       )         J     S     e     l  �   p  I     q   \  r   �     A     M     k  	   }  "   �  
   �     �     �     �     �  =   �  &   #     J     P     d     w     �     �  H   �  A        D     M  ?   R     �     �     �     �  @   �            (   +     T  5   l     �  5   �     �                        9   +      5      )      "       -   7   $   4   2   (      :             !   .          6       #   ,      3                              1       0   	           %             '      
                    *      /              &                                 8    A proper e-mail address is required. Activated plugins Additional options Attachment is broken. Attachment: Change label for fields of the contact form Coming from (referer) Contact Form Contact Form Options Contact from Date/Time Display Attachment block Display Send me a copy block Download E-Mail Address: Email FAQ If information in the below fields are empty then the message will be send to an address which was specified during registration. If you can see this MIME than your client doesn't accept MIME types! If you have any questions, please contact us via plugin@bestwebsoft.com or fill in our contact form on our site If you would like to add a Contact Form to your website, just copy and put this shortcode onto your post or page: Install %s Install now from wordpress.org Installed plugins Message Message text is required. Message: Name Name: Options saved. Please complete the CAPTCHA. Please input correct email. Settings are not saved. Please make corrections below and try again. Read more Recommended plugins Save Changes Select user name Send me a copy Sent from (ip address) Set a name of user wo will get messages from a contact form. Set an email address which will be used for messages receiving. Settings Site Sorry, your e-mail could not be delivered. Subject Subject text is required. Subject: Submit Such user is not exist. Settings are not saved. Support Thank you for contacting us. Use email of wordpress user: Use this email: Users can attach files of the following types Using (user agent) You can attach files of the following types Your name is required. Project-Id-Version: contact_form
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2012-01-04 12:10+0200
PO-Revision-Date: 2012-01-04 12:10+0200
Last-Translator: BWS <zos@bestwebsoft.com>
Language-Team: bestwebsoft.com <plugin@bestwebsoft.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: __;_e
X-Poedit-Basepath: ..
X-Poedit-Language: Italian
X-Poedit-Country: ITALY
X-Poedit-SourceCharset: utf-8
X-Poedit-SearchPath-0: .
 È richiesto un indirizzo e-mail corretto Plugin attivati Impostazioni aggiuntive L'attachment non è corretto Attachment: Modifica le etichette dei campi del contact form Da (referente) Contact Form Opzioni Contact Form Contact from Data/Ora Visualizza l'attachment Visualizza il blocco "Inviami una copia"  Download Indirizzo e-mail: e-mail FAQ Se le informazioni nei campi sottostanti sono vuote allora il messaggio sarà inviato ad un indirizzo che è stato specificato durante la fase di registrazione.  Se puoi visualizzare questo MIME il tuo client non supporta i MIME types! Se hai domande, per favore contattaci a plugin@bestwebsoft.com o compila il modulo "contact form" sul nostro sito Se vuoi aggiungere un Contact Form al tuo sito, copia e incolla questo shortcode nei tuoi post o nelle tue pagine: Installa %s Installa ora da wordpress.org Plugin installati Messaggio Il campo messaggio è obbligatorio Messaggio: Nome Nome: Opzioni salvate. Completa il CAPTCHA Perfavore inserisci una e-mail corretta. Opzioni non salvate. Controlla i dati del modulo e riprova! Leggi Plugin raccomandati Salva le modifiche Seleziona nome utente Inviami una copia Inviato da (indirizzi IP) Imposta il nome dell'utente che riceverà i messaggi da un contact form. Imposta una e-mail che verrà utilizzata per ricevere i messaggi. Settaggi Sito Spiacenti, la tua e-mail non può essere consegnata al momento. Oggetto L'oggetto è obbligatorio Oggetto: Invia L'utente indicato non esiste. Le opzioni non sono state salvate. Supporto Grazie per averci contattato. Utilizza l'e-mail dell'utente wordpress: Utilizza questa e-mail: Gli utenti possono allegare i files nei seguenti tipi Utilizza (user agent) Gli utenti possono allegare i files nei seguenti tipi Il nome è obbligatorio 